FROM php:apache

RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod env 

ARG ENV

ADD public/ /var/www/html
ADD src/ /var/www/src/
ADD templates/ /var/www/templates/
ADD composer.json /var/www/composer.json
ADD composer.lock /var/www/composer.lock
ADD build/php.ini /usr/local/etc/php/php.ini
RUN mkdir /var/www/logs && chmod 777 /var/www/logs


RUN echo "SetEnv ENV $ENV" >> /var/www/html/.htaccess

RUN apt-get update && apt-get install openssl libssl-dev git curl libpcre3-dev -y
RUN pecl install mongodb

WORKDIR /var/www
# Setup the Composer installer
RUN curl -s http://getcomposer.org/installer | php
RUN php composer.phar install

WORKDIR /var/www/html

EXPOSE 80

