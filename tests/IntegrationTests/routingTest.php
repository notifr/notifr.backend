<?php

namespace Tests\IntegrationTests;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

class HomepageTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return \Slim\Http\Response
     */
    public function runApp($requestMethod, $requestUri, $requestData = null, $dependencies = array())
    {
        // Create a mock environment for testing with
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri
            ]
        );

        // Set up a request object based on the environment
        $request = Request::createFromEnvironment($environment);

        // Add request data, if it exists
        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        // Set up a response object
        $response = new Response();

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__ . '/../../src/dependencies.php';

        // Register controllers
        require_once __DIR__ . '/../../src/controllers/controller.php';
        require_once __DIR__ . '/../../src/controllers/userController.php';
        require_once __DIR__ . '/../../src/controllers/subscriptionController.php';

        // Register services
        require_once __DIR__ . '/../../src/services/dbHandler.php';

        // Register routes
        require __DIR__ . '/../../src/routes.php';

        // set dependencies
        $container = $app->getContainer();
        foreach ($dependencies as $key => $value) {
            $app->key = $value;
            $container[$key] = function ($c) {
                //$settings = $c['settings']['db'];
                //$client = new MongoDB\Client("mongodb://" . $settings['host']);
                //return new \Notifr\Services\DbHandler($client->{$settings['dbname']});
                return $value;
            };
        }

        // Process the application
        $response = $app->process($request, $response);

        // Return the response
        return $response;
    }

    public function testCorsRules() {
        $response = $this->runApp('POST', '/api');

        $this->assertEquals(
            '*',
            $response->getHeader('Access-Control-Allow-Origin')[0]
        );
        $this->assertEquals(
            'X-Requested-With, Content-Type, Accept, Origin, Authorization, Location', 
            $response->getHeader('Access-Control-Allow-Headers')[0]
        );
        $this->assertEquals(
            'GET, POST, PUT, DELETE, OPTIONS', 
            $response->getHeader('Access-Control-Allow-Methods')[0]
        );
    }

    public function testOptionsCall() {
        $response = $this->runApp('OPTIONS', '/api');
        $this->assertInstanceOf(Response::class, $response);
    }

    public function testAddNewUserWithoutParameters() {
        $response = $this->runApp('POST', '/api/users/');

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"request could not be parsed correctly"}', (string)$response->getBody());
    }


    public function testReceiveSubscriptionWithInvalidData1() {
        $data = array(
            "connector" => "bands",
            "userid" => "58e11854d0d5a5d61369c824",
            "message" => "test"
        );

        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"could not create subscription"}', (string)$response->getBody());
    }

    public function testReceiveSubscriptionWithInvalidData2() {
        $data = array(
            "connector" => "bands",
            "userid" => "58e11854d0d5a5d61369c824",
            "data" => array(
                "band" => "selena"
            )
        );

        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"could not create subscription"}', (string)$response->getBody());
    }

    public function testReceiveSubscriptionWithInvalidData3() {
        $data = array(
            "userid" => "58e11854d0d5a5d61369c824",
            "message" => "test",
            "data" => array(
                "band" => "selena"
            )
        );
        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"could not create subscription"}', (string)$response->getBody());
    }

    public function testReceiveSubscriptionWithInvalidData4() {
        $data = array(
            "connector" => "bands",
            "message" => "test",
            "data" => array(
                "band" => "selena"
            )
        );
        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"could not create subscription"}', (string)$response->getBody());
    }

    public function testReceiveSubscriptionWithInvalidData5() {
        $data = array();
        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"could not create subscription"}', (string)$response->getBody());
    }

    public function testReceiveSubscriptionWithValidData() {
        $data = array(
            "connector" => "bands",
            "userid" => "58e11854d0d5a5d61369c824",
            "message" => "test",
            "data" => array(
                "band" => "selena"
            )
        );

        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testDeleteSubscriptionWithNoSubscription1() {
        $response = $this->runApp('DELETE', '/api/subscriptions/58e11854d0d5a5d61369c824');
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('{"message":"no subscription found"}', (string)$response->getBody());
    }

    public function testDeleteSubscriptionWithNoSubscription2() {
        $response = $this->runApp('DELETE', '/api/subscriptions/1');
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('{"message":"there was an error"}', (string)$response->getBody());
    }

    public function testDeleteSubscriptionWithValidData() {
        $data = array(
            "connector" => "bands",
            "userid" => "58e11854d0d5a5d61369c824",
            "message" => "test",
            "data" => array(
                "band" => "selena"
            )
        );

        $response = $this->runApp('POST', '/api/subscriptions/', $data);
        $id = substr($response->getHeader('Location')[0], 19);

        $response = $this->runApp('DELETE', '/api/subscriptions/'. $id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testReceiveNotificationWithInvalidData1() {
        $data = array(
            "message" => "notify",
            "data" => array(
                "notificationMessage" => "Selena ist am 21.03 im Hallenstadion !",
            )
        );

        $response = $this->runApp('POST', '/api/notifications/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testReceiveNotificationWithInvalidData2() {
        $data = array(
            "data" => array(
                "notificationMessage" => "Selena ist am 21.03 im Hallenstadion !",
            )
        );

        $response = $this->runApp('POST', '/api/notifications/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testReceiveNotificationWithInvalidData3() {
        $data = array(
            "message" => "notify",
        );

        $response = $this->runApp('POST', '/api/notifications/', $data);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testGetSubscriptionsByConnectorWithValidData() {
        $response = $this->runApp('GET', '/api/connectors/bands/subscriptions/');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testGetSubscriptionsByUserIdWithInvalidData() {
        $response = $this->runApp('GET', '/api/users/58d97c05f5ec4e/subscriptions');
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    public function testGetNotificationsByUserIdWithInvalidData() {
        $response = $this->runApp('GET', '/api/users/58d97c05f5ec4e/notifications');
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
    }

    /*public function testAddNewUserWithLegalParameters1() {
        $response = $this->runApp('POST', '/api/users/', array("device_type" => 0, 'identifier' => 'someidentifier'));

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"user created"}', (string)$response->getBody());
    }*/

    public function testAddNewUserWithLegalParameters2() {
        $response = $this->runApp('POST', '/api/users/', array("device_type" => 0, 'identifier' => 'SIMULATOR_TOKEN'));

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertContains('application/json', $response->getHeader('Content-Type')[0]);
        $this->assertContains('{"message":"user created"}', (string)$response->getBody());
    }
}