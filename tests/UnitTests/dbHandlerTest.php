<?php

namespace Tests\UnitTests;
use Slim\App;
use Notifr\Controllers;

class DbHandlerTest extends \PHPUnit_Framework_TestCase
{
    private $db;

    /**
     * Constructor
     */
    public function __construct()
    {

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__ . '/../../src/dependencies.php';

        // set dependencies
        $container = $app->getContainer();
        
        require_once __DIR__ . '/../../src/services/dbHandler.php';

        $this->db = $container['db'];
    }

    /**
     * Tests if a user can be added
     */
    public function testAddUser()
    {
        $data = array("onesignal_id" => 'test_id');
        $user = $this->db->addUser($data);
        $this->assertInstanceOf(\MongoDB\InsertOneResult::class, $user);
    }

    /**
     * Tests if a user can be retrieved
     */
    public function testGetUserById()
    {
        $user = $this->db->getUserById("58dfcc43b009586149567f92");
        $this->assertInstanceOf(\MongoDB\Driver\Cursor::class, $user);
    }

    /**
     * Tests if a subscription can be added
     */
    public function testAddSubscription()
    {
        $data = array("test" => "data");
        $user = $this->db->addSubscription($data);
        $this->assertInstanceOf(\MongoDB\InsertOneResult::class, $user);
    }

    /**
     * Tests if a notficiation can be added
     */
    public function testAddNotification()
    {
        $data = array("test" => "data");
        $user = $this->db->addNotification($data);
        $this->assertInstanceOf(\MongoDB\InsertOneResult::class, $user);
    }

    /**
     * Tests if a subscription can be deleted
     */
    public function testDeleteSubscription()
    {
        $data = array("_id" => "58dfcc43b009586149567f92");
        $user = $this->db->deleteSubscription($data);
        $this->assertInstanceOf(\MongoDB\DeleteResult::class, $user);
    }

    /**
     * Tests if a subscription can found by connector
     */
    public function testGetSubscriptionsByConnector()
    {
        $user = $this->db->getSubscriptionsByConnector("bands");
        $this->assertInstanceOf(\MongoDB\Driver\Cursor::class, $user);
    }

    /**
     * Tests if a subscription can found by id
     */
    public function testGetSubscriptionsById()
    {
        $user = $this->db->getSubscriptionById("58dfcc43b009586149567f92");
        $this->assertInstanceOf(\MongoDB\Driver\Cursor::class, $user);
    }

    /**
     * Tests if a subscription can found by userid
     */
    public function testGetSubscriptionsByUserId()
    {
        $user = $this->db->getSubscriptionsByUserId("58dfcc43b009586149567f92");
        $this->assertInstanceOf(\MongoDB\Driver\Cursor::class, $user);
    }

    /**
     * Tests if a notification can found by userid
     */
    public function testGetNotificationsById()
    {
        $user = $this->db->getNotificationsByUserId("58dfcc43b009586149567f92");
        $this->assertInstanceOf(\MongoDB\Driver\Cursor::class, $user);
    }


}