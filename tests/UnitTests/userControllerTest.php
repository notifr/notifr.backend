<?php

namespace Tests\UnitTests;

use Notifr\Controllers;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{
    private $userController;

    /**
     * Constructor
     */
    public function __construct() {
        require_once __DIR__ . '/../../src/controllers/controller.php';
        require_once __DIR__ . '/../../src/controllers/userController.php';

        $this->userController = new \Notifr\Controllers\UserController(null, null, null);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }


    /**
     * Tests valid user input data
     */
    public function testIsUserDataValidWithValidData()
    {
        $data = array('device_type' => 0, 'identifier' => 'abcd');
        $this->assertTrue($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));
    }

    /**
     * Tests invalid user input data
     */
    public function testIsUserDataValidWithInvalidData()
    {
        $data = array('device_type' => 0);
        $this->assertFalse($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));

        $data = array('identifier' => 'abcd');
        $this->assertFalse($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));

        $data = array();
        $this->assertFalse($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));

        $data = null;
        $this->assertFalse($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));

        $data = false;
        $this->assertFalse($this->invokeMethod($this->userController, 'isUserDataValid', array($data)));
    }

    /**
     * Tests valid device data
     */
    public function testIsDeviceDataValidWithValidData()
    {
        $data = array('id' => '123');
        $this->assertTrue($this->invokeMethod($this->userController, 'isDeviceDataValid', array($data)));
    }

    /**
     * Tests invalid device input data
     */
    public function testIsDeviceDataValidWithInvalidData()
    {
        $data = array();
        $this->assertFalse($this->invokeMethod($this->userController, 'isDeviceDataValid', array($data)));

        $data = null;
        $this->assertFalse($this->invokeMethod($this->userController, 'isDeviceDataValid', array($data)));

        $data = false;
        $this->assertFalse($this->invokeMethod($this->userController, 'isDeviceDataValid', array($data)));
    }

}