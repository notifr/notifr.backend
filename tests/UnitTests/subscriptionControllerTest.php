<?php

namespace Tests\UnitTests;

use Notifr\Controllers;

class SubscriptionControllerTest extends \PHPUnit_Framework_TestCase
{
    private $subscriptionController;

    /**
     * Constructor
     */
    public function __construct() {
        require_once __DIR__ . '/../../src/controllers/controller.php';
        require_once __DIR__ . '/../../src/controllers/subscriptionController.php';

        $this->subscriptionController = new \Notifr\Controllers\SubscriptionController(null, null, null, null);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array()) {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }


    /**
     * Tests valid subscription input data
     */
    public function testIsValidSubscriptionDataWithValidData() {
        $data = array(
            'connector' => "somestring",
            'userid' => 15,
            "message" => "some message",
            "data" => array(
                "band" => "some string",
                "country" => "some country"
            )
        );
        $this->assertTrue($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));

        $data = array(
            'connector' => "somestring",
            'userid' => 15,
            "message" => "some message",
            "data" => array()
        );
        $this->assertTrue($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));
    }

    /**
     * Tests invalid subscription input data
     */
    public function testIsUserDataValidWithInvalidData() {
        $data = array(
            'userid' => 15,
            "message" => "some message",
            "data" => array(
                "band" => "some string",
                "country" => "some country"
            )
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));

        $data = array(
            'connector' => "somestring",
            "message" => "some message",
            "data" => array(
                "band" => "some string",
                "country" => "some country"
            )
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));

        $data = array(
            'connector' => "somestring",
            'userid' => 15,
            "data" => array(
                "band" => "some string",
                "country" => "some country"
            )
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));

        $data = array(
            'connector' => "somestring",
            'userid' => 15,
            "message" => "some message"
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));
    }

    /**
     * Tests invalid subscription input data
     */
    public function testIsUserDataValidWithNull() {
        $data = null;
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));
    }

    /**
     * Tests invalid subscription input data
     */
    public function testIsUserDataValidWithFalse() {
        $data = false;
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidSubscriptionData', array($data)));
    }

    /**
     * Tests valid notification input data
     */
    public function testIsValidNotificationDataWithValidData() {
        $data = array(
            "message" => "notify",
            "data" => array(
                "notificationMessage" => "some string",
                "subscriptions" => array("test")
            )
        );
        $this->assertTrue($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));
    }

    /**
     * Tests invalid subscription input data
     */
    public function testIsNotificationDataValidWithInvalidData() {
        $data = array(
            "message" => "notify",
            "data" => array()
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));

        $data = array(
            "data" => array(
                "notificationMessage" => "some string"
            )
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));

        $data = array(
            "message" => "notify"
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));

        $data = array(
            "message" => "notify",
            "data" => array(
                "notificationMessage" => null
            )
        );
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));
    }

    /**
     * Tests invalid notification input data
     */
    public function testIsNotificationDataValidWithNull() {
        $data = null;
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));
    }

    /**
     * Tests invalid notification input data
     */
    public function testIsNotificationDataValidWithFalse() {
        $data = false;
        $this->assertFalse($this->invokeMethod($this->subscriptionController, 'isValidNotificationData', array($data)));
    }

}