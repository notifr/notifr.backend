<?php

namespace Tests\UnitTests;
use Slim\App;
use Notifr\Services;
use Pubnub\Pubnub;
use OneSignal\Config;
use OneSignal\Devices;
use OneSignal\OneSignal;
use Notifr\Controllers;



class DependenciesTest extends \PHPUnit_Framework_TestCase
{
    private $container;
    private $subscriptionController;
    private $userController;
    private $pubnub;
    private $onesignal;

    /**
     * Constructor
     */
    public function __construct()
    {

        // Use the application settings
        $settings = require __DIR__ . '/../../src/settings.php';

        // Instantiate the application
        $app = new App($settings);

        // Set up dependencies
        require __DIR__ . '/../../src/dependencies.php';

        // set dependencies
        $this->container = $app->getContainer();
        
        require_once __DIR__ . '/../../src/services/dbHandler.php';

        
    }

    /*
     * Tests the Subscription Container
     */
    public function testSubscriptionContainer()
    {
        $this->subscriptionController = $this->container['SubscriptionController'];

        $this->assertInstanceOf(
            \Notifr\Controllers\SubscriptionController::class,
            $this->subscriptionController
        );
    }

    /*
     * Tests the User Container
     */
    public function testUserContainer()
    {
        $this->userController = $this->container['UserController'];

        $this->assertInstanceOf(
            \Notifr\Controllers\UserController::class,
            $this->userController
        );
    }

    /*
     * Tests the Pubnub Container
     */
    public function testPubnubContainer()
    {
        $this->pubnub = $this->container['pubnub'];

        $this->assertInstanceOf(
            Pubnub::class,
            $this->pubnub
        );
    }

    /*
     * Tests the OneSignal Container
     */
    public function testOneSignalContainer()
    {
        $this->onesignal = $this->container['onesignal'];

        $this->assertInstanceOf(
            OneSignal::class,
            $this->onesignal
        );
    }

}