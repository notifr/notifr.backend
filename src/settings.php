<?php

$dbhost = 'mongodb-test:27017';
$dbname = 'notifr';

// check for prod or test env
if (isset($_SERVER['ENV']) && $_SERVER['ENV'] == 'prod') {
    $dbhost = 'mongodb-prod:27017';
    $dbname = 'notifr';
}
if (isset($_SERVER['ENV']) && $_SERVER['ENV'] == 'test') {
    $dbhost = 'mongodb-test:27017';
    $dbname = 'notifr';
}

// check for local dev
if (array_key_exists('HTTP_HOST', $_SERVER)
    && $_SERVER['HTTP_HOST'] == 'localhost') {
    $dbhost = 'localhost:27017';
}
// check for unit tests
if (PHP_SAPI == 'cli') {
    $dbhost = 'localhost:27017';
    $dbname = 'test';
}

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        'db' => [
            'host' => $dbhost,
            'dbname' => $dbname,
        ],

        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'pubnub' => [
            'subscribe_key' => 'sub-c-e0db07c2-12f6-11e7-bd0e-0619f8945a4f',
            'publish_key' => 'pub-c-0caad1c8-27fa-4c1c-beda-da46d79818d7'
        ],

        'onesignal' => [
            'application_id' => 'efa88578-1abe-4ff7-b54b-ca432906f48a',
            'application_auth_key' => 'OGNhN2JhMjQtYjBkZS00MGE3LWFjNzYtY2RlODlkY2RiZDNi',
            'your_auth_key' => 'MjI4NWM5ZGEtZjJiOS00NzA2LWFkODAtNDAxNWY0Zjk5MmIw'
        ]
    ],
];
