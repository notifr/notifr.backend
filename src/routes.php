<?php
// Routes

// enable cors
$app->options('/{routes:.+}', function ($response) {
    return $response;
});
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, Location')
        ->withHeader('Access-Control-Expose-Headers', 'Location')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});



$app->group('/api', function() {

    $this->group('/users', function() {
        $this->post('/', 'UserController:addUser');
        $this->get('/{userId}/notifications', 'SubscriptionController:getNotificationsByUserId');


        $this->get('/{userId}/subscriptions', 'SubscriptionController:getSubscriptionsByUserId');
    });

    $this->group('/subscriptions', function() {
        $this->post('/', 'SubscriptionController:receiveSubscription');
        $this->delete('/{subscriptionId}', 'SubscriptionController:deleteSubscription');
    });

    $this->post('/notifications/', 'SubscriptionController:receiveNotifications');
    $this->get('/connectors/{connectorId}/subscriptions/', 'SubscriptionController:getSubscriptionsByConnector');
});












