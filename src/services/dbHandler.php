<?php

namespace Notifr\Services;


/**
 * Handles all CRUD operations.
 */
class DbHandler {
    private $db;

    /**
     * Constructor
     *
     * @param db    db object
     */
    public function __construct($db) {
        $this->db = $db;
    }

    /**
     * Adds a new user
     *
     * @param data    user data
     * @return colleciton    collection object
     */
    public function addUser($data) {
        $collection = $this->db->users;
        return $collection->insertOne( $data );
    }

    /**
     * Get user by id
     *
     * @param id    user id
     * @return colleciton    collection object
     */
    public function getUserById($id) {
        $collection = $this->db->users;
        return $collection->find(array("_id" => new \MongoDB\BSON\ObjectID($id)));
    }

    /**
     * Adds a new subscription
     *
     * @param data    subscription data
     * @return colleciton    collection object
     */
    public function addSubscription($data) {
        $collection = $this->db->subscriptions;
        return $collection->insertOne( $data );
    }

    /**
     * Adds a new notification
     *
     * @param data    notification data
     * @return colleciton    collection object
     */
    public function addNotification($data) {
        $collection = $this->db->notifications;
        return $collection->insertOne( $data );
    }

    /**
     * Delete a subscription
     *
     * @param data    subscription data
     * @return colleciton    collection object
     */
    public function deleteSubscription($data) {
        $collection = $this->db->subscriptions;
        return $collection->deleteOne( $data );
    }

    /**
     * Get a subscription by connector
     *
     * @param connector    connector name
     * @return colleciton    collection object
     */
    public function getSubscriptionsByConnector($connector) {
        $collection = $this->db->subscriptions;
        return $collection->find(array("connector" => $connector));
    }

    /**
     * Get a subscription by id
     *
     * @param id    subscription id
     * @return colleciton    collection object
     */
    public function getSubscriptionById($id) {
        $collection = $this->db->subscriptions;
        return $collection->find(array("_id" => new \MongoDB\BSON\ObjectID($id)));
    }

    /**
     * Get a subscription by user id
     *
     * @param id    user id
     * @return colleciton    collection object
     */
    public function getSubscriptionsByUserId($id) {
        $collection = $this->db->subscriptions;
        return $collection->find(array("userid" => new \MongoDB\BSON\ObjectID($id)));
    }

    /**
     * Get a notification by user id
     *
     * @param id    user id
     * @return colleciton    collection object
     */
    public function getNotificationsByUserId($id) {
        $collection = $this->db->notifications;
        return $collection->find(array("userid" => new \MongoDB\BSON\ObjectID($id)));
    }
}