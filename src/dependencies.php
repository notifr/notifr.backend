<?php

use Notifr\Services;
use Pubnub\Pubnub;
use OneSignal\Config;
use OneSignal\Devices;
use OneSignal\OneSignal;
use Notifr\Controllers;

$container = $app->getContainer();

$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['db'] = function ($c) {
    $settings = $c['settings']['db'];
    $client = new MongoDB\Client("mongodb://" . $settings['host']);
    return new \Notifr\Services\DbHandler($client->{$settings['dbname']});
};

$container['pubnub'] = function($c) {
    $settings = $c['settings']['pubnub'];
    return new Pubnub($settings['publish_key'], $settings['subscribe_key']);
};

$container['onesignal'] = function($c) {
    $settings = $c['settings']['onesignal'];
    $config = new Config();
    $config->setApplicationId($settings['application_id']);
    $config->setApplicationAuthKey($settings['application_auth_key']);
    $config->setUserAuthKey($settings['your_auth_key']);
    return new OneSignal($config);
};

$container['UserController'] = function ($c) {
    $db = $c->get("db");
    $onesignal = $c->get("onesignal");
    $logger = $c->get("logger");
    return new \Notifr\Controllers\UserController($db, $onesignal, $logger);
};

$container['SubscriptionController'] = function ($c) {
    $db = $c->get("db");
    $pubnub = $c->get("pubnub");
    $onesignal = $c->get("onesignal");
    $logger = $c->get("logger");
    return new \Notifr\Controllers\SubscriptionController($db, $pubnub, $onesignal, $logger);
};









