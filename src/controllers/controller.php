<?php

namespace Notifr\Controllers;

class Controller {

    protected function createDefaultResponse($response) {
        return $response->withStatus(400)
                        ->withHeader('Content-type', 'application/json');
    }
}