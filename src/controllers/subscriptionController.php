<?php

namespace Notifr\Controllers;

class SubscriptionController extends Controller {

    protected $db;
    protected $pubnub;
    protected $onesignal;
    protected $logger;

    const PARSE_ERROR = 'request could not be parsed correctly';
    const SUBSCRIPTION_FETCH_ERROR = "could not fetch subscriptions";
    const MSG = 'message';
    const CONNECTOR = 'connector';
    const CONNECTOR_ID = 'connectorId';
    const SUBSCRIPTION_ID = 'subscriptionId';
    const USER_ID = 'userid';
    const SUBSCRIPTION_MSG = "message";
    const SIMULATOR_TOKEN = "SIMULATOR_TOKEN";
    const CREATE_DATE = "create_date";
    const NOTIFICATION_MSG = 'notificationMessage';
    
    /**
     * Constructor
     *
     * @param db
     * @param pubnub
     */
    public function __construct($db, $pubnub, $onesignal, $logger) {
        $this->db = $db;
        $this->pubnub = $pubnub;
        $this->onesignal = $onesignal;
        $this->logger = $logger;
    }

    /**
     * This method receives a request from the app for a new subscription.
     * It's arranging the persistens of the subscription and its publication.
     *
     * Format of receiving JSON:
     * {
     *      "connector": "bands",
     *      "userid" : "58fcb30a21ff6200104892d2", // must be in format '58....'. id must be existing or it won't work
     *      "message" : "Werde informiert wenn Selena ein Konzert hat."
     *      "data": {
     *          "band": "selena"
     *      }
     * }
     * 
     * @param request
     * @param response
     * @return response    contains http status 201 or 400
     */
    public function receiveSubscription($request, $response) {
        $default_response = parent::createDefaultResponse($response);
        $this->logger->info('received new subscription with params: ' . var_export($request->getParsedBody(), true));

        try {
            $subscription = $this->addSubscription($request);

            $path = $request->getUri()->getPath();
            $response = $default_response->withStatus(201)
                                         ->withHeader('Location', $path . $subscription['id'])
                                         ->withJson(array(self::MSG => "subscription created"));
            $this->publishSubscription($subscription);
            $this->logger->info('published subscription');

        } catch (\Exception $e) {
            $response = $default_response->withJson(array(self::MSG => "could not create subscription"));
        }

        return $response;
    }

    /**
     * Deletes a subscription and it's message
     *
     * @param request
     * @param response
     * @param args
     * @return response    contains http status 200 or 400
     */
    public function deleteSubscription($request, $response, $args) {
        $default_response = parent::createDefaultResponse($response);
        $this->logger->info('deleting subscription with id: ' . $args[self::SUBSCRIPTION_ID] . ' ...');
        try {
            // get connector of subscription
            $subscription = $this->db->getSubscriptionById($args[self::SUBSCRIPTION_ID]);
            $subscription = iterator_to_array($subscription);

            if (isset($subscription[0][self::CONNECTOR])) {
                $this->db->deleteSubscription(array("_id" => new \MongoDB\BSON\ObjectID($args[self::SUBSCRIPTION_ID])));
                $this->logger->info('deleted subscription from db');

                $msg = '{'
                    .'"message":"unsubscribe",'
                    .'"data":{"subscriptions":[{ "id": "'. $args[self::SUBSCRIPTION_ID] .'" }]}'
                .'}';
                $this->pubnub->publish($subscription[0][self::CONNECTOR], $msg);
                $this->logger->info('unsubscribed subscription via pubnub');

                $response = $default_response->withStatus(200)
                                             ->write($msg);
            } else {
                $response = $default_response->withJson(array(self::MSG => "no subscription found"));
            }
        } catch (\Exception $e) {
            $response = $default_response->withJson(array(self::MSG => 'there was an error'));
        }
        return $response;
    }

    /**
     * Receives a message and subscription ids to notify
     *
     * Format of receiving JSON:
     * {
     *      "message": "notify",
     *      "data": {
     *          "notificationMessage": "Selena Gomez ist am 21.03.2017 im Hallenstadion Zürich!",
     *          "subscriptions": [
     *              { "id": "FIRST_SUBSCRIPTION_TO_NOTIFY" },
     *              { "id": "SECOND_SUBSCRIPTION_TO_NOTIFY" }
     *          ]
     *      }
     *  }
     *
     * @param request
     * @param response
     * @return response    contains http status 200 or 400
     */
    public function receiveNotifications($request, $response) {
        $default_response = parent::createDefaultResponse($response);
        $data = $request->getParsedBody();
        $this->logger->info('received new notification with params: ' . var_export($data, true));

        if ($this->isValidNotificationData($data)) {
            try {
                $notificationData = $this->preprareNotificationData($data['data']);
                $this->sendNotification($notificationData);

                $response = $default_response->withStatus(200)
                                             ->withJson(array(self::MSG => "received and sent notifications"));
            } catch (\Exception $e) {
                $response = $default_response->withJson(array(self::MSG => self::SUBSCRIPTION_FETCH_ERROR));
            }
        } else {
            $response = $default_response->withJson(array(self::MSG => self::PARSE_ERROR));
        }
        return $response;
    }

    /**
     * Prepares data for sending notification
     *
     * @param data    contains data for notification
     * @return opt    contains data for sending notification
     */
    private function preprareNotificationData($data) {
        $player_ids = array();
        $timestamp = new \MongoDB\BSON\UTCDateTime(new \DateTime);
        foreach ($data['subscriptions'] as $val) {
            $subscription = $this->db->getSubscriptionById($val['id']);
            $subscription = iterator_to_array($subscription);

            $user = $this->db->getUserById((string)$subscription[0][self::USER_ID]);
            $user = iterator_to_array($user);

            if (self::SIMULATOR_TOKEN != $user[0]['pushtoken']) {
                array_push($player_ids, $user[0]['onesignal_id']);
            }
            $notification = array(
                "userid" => $subscription[0][self::USER_ID],
                "message" => $data[self::NOTIFICATION_MSG],
                "connector" => $subscription[0][self::CONNECTOR],
                self::CREATE_DATE => $timestamp
            );
            $this->db->addNotification($notification);
            $this->logger->info('added new notification: ' . var_export($notification, true));
        }

        $opt = array(
            'app_id' => $this->onesignal->getConfig()->getApplicationId(),
            'include_player_ids' => $player_ids, //Limit of player ids 2,000 entries per REST API call
            'contents' => array('en' => $data[self::NOTIFICATION_MSG]),
        );
        return $opt;
    }

    /**
     * Sends notification
     *
     * @param data    contains data for sending notification 
     */
    private function sendNotification($data) {
        // only send notifications when there is at least one real user
        if (count($data['include_player_ids']) > 0) {
            $this->onesignal->notifications->add($data);
        }
        $this->logger->info('notification sent to device');
    }

    /**
     * Returns all subscriptions from a specific connector
     *
     * @param request
     * @param response
     * @return response    contains http status 201 or 400
     */
    public function getSubscriptionsByConnector($request, $response, $args) {
        $default_response = parent::createDefaultResponse($response);
        $this->logger->info('get subscription by connector with id: ' . $args[self::CONNECTOR_ID]);

        try {
            $subscriptions = $this->db->getSubscriptionsByConnector($args[self::CONNECTOR_ID]);
            $msg = '{'
                .'"message":"subscriptions",'
                .'"data":{'
                    .'"subscriptions":['. $this->prepareSubscriptionsAsJSON($subscriptions) .']'
                .'}'
            .'}';

            $response = $default_response->withStatus(200)
                                         ->write($msg);
        } catch (\Exception $e) {
            $response = $default_response->withJson(array(self::MSG => self::SUBSCRIPTION_FETCH_ERROR));
        }
        return $response;
    }

    /**
     * Returns all subscriptions from a specific user
     *
     * @param request
     * @param response
     * @return response    contains http status 200 or 400
     */
    public function getSubscriptionsByUserId($request, $response, $args) {
        $default_response = parent::createDefaultResponse($response);
        $this->logger->info('get subscription by user with id: ' . $args['userId']);

        try {
            $subscriptions = $this->db->getSubscriptionsByUserId($args['userId']);
            $subscriptions = array_reverse(iterator_to_array($subscriptions));

            $result = '{ "message": "subscriptions", "data": [';
            for ($i=0; $i < count($subscriptions); $i++) {
                $subscription = $subscriptions[$i];
                $subscription['_id'] = (string)$subscription['_id'];
                $subscription[self::USER_ID] = (string)$subscription[self::USER_ID];

                $result .= json_encode($subscription);
                if ($i != count($subscriptions) - 1) {
                    $result .= ',';
                }
            }
            $result .= ']}';
            $response = $default_response->withStatus(200)
                                         ->write($result);
        } catch (\Exception $e) {
            $response = $default_response->withJson(array(self::MSG => self::SUBSCRIPTION_FETCH_ERROR));
        }
        return $response;
    }

    /**
     * Returns all notifications from a specific user
     *
     * @param request
     * @param response
     * @return response    contains http status 200 or 400
     */
    public function getNotificationsByUserId($request, $response, $args) {
        $default_response = parent::createDefaultResponse($response);
        $this->logger->info('get notification by user with id: ' . $args['userId']);

        try {
            $notifications = $this->db->getNotificationsByUserId($args['userId']);
            $notifications = array_reverse(iterator_to_array($notifications));

            $result = '{ "message": "notifications", "data": [';
            for ($i=0; $i < count($notifications); $i++) {
                $notification = $notifications[$i];
                $notification['id'] = (string)$notification['_id'];
                $notification[self::CREATE_DATE] = (string)$notification[self::CREATE_DATE];

                unset($notification['_id']);
                unset($notification[self::USER_ID]);

                $result .= json_encode($notification);
                if ($i != count($notifications) - 1) {
                    $result .= ',';
                }
            }
            $result .= ']}';
            $response = $default_response->withStatus(200)
                                         ->write($result);
        } catch (\Exception $e) {
            $response = $default_response->withJson(array(self::MSG => 'could not fetch notifications'));
        }
        return $response;
    }


    /**
     * Converts the subscriptions in the needed JSON-format for the connector
     * Also removes unnessesary fields.
     *
     * @param subscriptions    collection of subscriptions
     * @return string    subscriptions as json
     */
    private function prepareSubscriptionsAsJSON($subscriptions) {
        $connected_subscriptions = "";

        // convert mongoCursor object to array
        $subscriptions = iterator_to_array($subscriptions);

        for ($i=0; $i < count($subscriptions); $i++) {
            $subscription = $subscriptions[$i];

            // remove unnessesary data
            unset($subscription[self::USER_ID]);
            unset($subscription[self::CONNECTOR]);

            $subscription['id'] = (string)$subscription['_id'];
            unset($subscription['_id']);
            $connected_subscriptions .= json_encode($subscription);

            if ($i != count($subscriptions) - 1) {
                $connected_subscriptions .= ',';
            }
        }

        return $connected_subscriptions;
    }

    /**
     * Saves a subscription to the database
     *
     * @param request
     * @return data    inserted data or null on failure
     */
    private function addSubscription($request) {
        $result = null;
        $data = $request->getParsedBody();
        $this->logger->info('add subscription with data: ' . var_export($data, true));


        if ($this->isValidSubscriptionData($data)) {
            $data[self::USER_ID] = new \MongoDB\BSON\ObjectID($data[self::USER_ID]);

            //unset($data['message']);

            $subscription = $this->db->addSubscription($data);
            $result = $data;
            $result['id'] = (string)$subscription->getInsertedId();
            $this->logger->info('added subscription in db');
        } else {
            throw new \InvalidArgumentException(self::PARSE_ERROR);
        }

        return $result;
    }

    /**
     * Publishes a subscripton for the connector
     *
     * @param subscription    contains the subscription data
     */
    private function publishSubscription($subscription) {
        $connector = $subscription[self::CONNECTOR];
        
        // remove unnessesary data
        unset($subscription[self::USER_ID]);
        unset($subscription[self::CONNECTOR]);

        $msg = '{'
            .'"message":"subscribe",'
            .'"data":{'
                .'"subscriptions":['. json_encode($subscription) .']'
            .'}'
        .'}';

        $this->pubnub->publish($connector, $msg);
    }

    /**
     * Checks if the received data has a valid structure
     * 
     * @param data   contains the subscription data
     * @return boolean    true on success else false
     */
    private function isValidSubscriptionData($data) {
        $condition = isset($data) && is_array($data);
        $condition = $condition && array_key_exists(self::CONNECTOR, $data) && $data[self::CONNECTOR] != '';
        $condition = $condition && array_key_exists(self::USER_ID, $data) && $data[self::USER_ID] != '';
        $condition = $condition && array_key_exists(self::SUBSCRIPTION_MSG, $data) && $data[self::SUBSCRIPTION_MSG] != '';
        return $condition && array_key_exists('data', $data);
    }

    /**
     * Checks if the received data has a valid structure
     * 
     * @param data   contains the notification data
     * @return boolean    true on success else false
     */
    private function isValidNotificationData($data) {
        $condition = isset($data) && is_array($data);
        $condition = $condition && array_key_exists('message', $data) && $data['message'] == 'notify';
        $condition = $condition && array_key_exists('data', $data) && !empty($data['data']);
        $condition = $condition && array_key_exists(self::NOTIFICATION_MSG, $data['data']);
        $condition = $condition && $data['data'][self::NOTIFICATION_MSG] != '';
        return $condition && !empty($data['data']['subscriptions']);
    }

}