<?php

namespace Notifr\Controllers;

class UserController extends Controller {

    protected $db;
    protected $onesignal;

    const MSG = 'message';
    const IDENTIFIER = 'identifier';
    const DEVICE_TYPE = 'device_type';

    
    /**
     * Constructor
     *
     * @param db
     * @param onesignal
     */
    public function __construct($db, $onesignal, $logger) {
        $this->db = $db;
        $this->onesignal = $onesignal;
        $this->logger = $logger;
    }

    /**
     * Adds a user on onesignal and creates a new user
     * in the database with user_id and onesignal_id
     *
     * Format of receiving JSON:
     * {
     *      "device_type" : 0,
     *      "identifier": "abcdef"
     *  }
     * 
     * @param request
     * @param response
     * @return response    contains http status 201 or 400
     */
    public function addUser($request, $response, $args) {
        $default_response = parent::createDefaultResponse($response);
        $data = $request->getParsedBody();
        $this->logger->info('received new user with params: ' . var_export($data, true));
        if ($this->isUserDataValid($data)) {
            $device_data = array( 
                'identifier' => $data[self::IDENTIFIER],
                'device_type' => (int)$data[self::DEVICE_TYPE], 
                "test_type" => 2
            );
            $onesignal_id = 'ONESINGNAL_ID';
            $pushtoken = 'SIMULATOR_TOKEN';
            if ($data[self::IDENTIFIER] != 'SIMULATOR_TOKEN') {
                $onesignal_id = $this->addOneSignalDevice($device_data);
                $pushtoken = $data[self::IDENTIFIER];
            }
            try {
                $result = $this->db->addUser(array('onesignal_id' => $onesignal_id, 'pushtoken' => $pushtoken));
                $response = $default_response->withStatus(201)
                                             ->withHeader('Location', $request->getUri()->getPath() . $result->getInsertedId())
                                             ->withJson(array(self::MSG => "user created"));
                $this->logger->info('added new user with: ' . var_export(array('id' => (string)$result->getInsertedId(), 'onesignal_id' => $onesignal_id, 'pushtoken' => $pushtoken), true));
            } catch (\Exception $e) {
                $response = $default_response->withJson(array(self::MSG => "could not create a device at one signal or save it in the database"));
            }
        } else {
            $response = $default_response->withJson(array(self::MSG => "request could not be parsed correctly"));
        }
        return $response;
    }

    /**
     *  Adds a device on one signal
     *  
     * @param $data      options for device
     * @return string    returns the one signal id
     */
    private function addOneSignalDevice($data) {
        $onesignal_response = $this->onesignal->devices->add($data);

        if ($this->isDeviceDataValid($onesignal_response)) {
            return $onesignal_response['id'];
        }
        throw new InvalidDeviceException("failed to create a device");
    }

    /**
     * Checks if the input for the new user is valid
     *
     * @param data    contains the user data
     * @return boolean    true if data is valid, else false
     */
    private function isUserDataValid($data) {
        $condition = isset($data) && is_array($data);
        $condition = $condition && array_key_exists(self::DEVICE_TYPE, $data) && isset($data[self::DEVICE_TYPE]);
        return $condition && array_key_exists(self::IDENTIFIER, $data) && $data[self::IDENTIFIER] != '';
    }

    /**
     * Checks if the input for the new onesignal device is valid
     *
     * @param data    contains the device data
     * @return boolean    true if data is valid, else false
     */
    private function isDeviceDataValid($data) {
        return isset($data) && is_array($data) && array_key_exists('id', $data) && !empty($data['id']);
    }
}