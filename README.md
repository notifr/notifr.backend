# notifR backend #
This is a short overview about the Backend of notifR

#### What is this backend for? ####
The functionality of the backend is do delegete and organize the communication between the app, the microservices and th cloud services.

#### Dependencies ####
* MongoDB incl. MongoDB driver for PHP

#### API Endpoints ####
* /api/users/ [POST]
* /api/users/{userId}/subscriptions [GET]
* /api/users/{userId}/notifications [GET]
* /api/subscriptions [POST]
* /api/subscriptions/{subscriptionId} [DELETE]
* /api/notifications/ [POST]
* /api/connectors/{connectorId}/subscriptions/ [GET]


